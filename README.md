# Plant32 - ESP32 based plant watering

This is the software part to control the ESP32Water hardware or similar hardware

Features:
* Up to 6 plant being monitored and individually watered
* fully autonomous operation
* remote control and monitoring via MQTT
* senses water delivery problems using a flow sensor
* watering limits and doses free configurable, store in non-volatile memory
* watchdog enabled operation to ensure maximum reliability


## Theory of operation

The water is dispensed through *outlets*, each outlet has an numeric id, starting with 1. An outlet is coupled to a *sensor*, a shared *pump* and a *valve*. 
The pump has in its water path a *flow meter*, which measures the actual amount of water flowing through the system. 


If the soil moisture level get low the valve of the outlet is opened, the pump started and a configurable amount of water is pumped to that outlet. If the measure amount has been reached, the pump is stopped, the valve is closed and for some time, no further watering will happen (by default 70 minutes). If the moisture is still to high, additional water is dispensed. There is a configurable amount of watering rounds (default 12). If after twelve watering rounds, the level has not been reached, watering of this plant is suspended for 12 hours. This should prevent overwatering the plant e.g. in case of a defective sensor. If in a watering action, the measured amount of water being dispensed is not reached within a certain timeframe, the pump assumes that either the water reservoir is empty or a connection is defective,  and goes into an "pump issue" state. No water is being dispensed until the user resets this state (remotely or via button press).  Usually this indicates empty water reservoir. This issue state is also visually indicated by the blinking led.

The sensor level which determines the intended soil moisture and the amount of water being pumped is configured via MQTT and stored in the device.

The system currently does not limit the time frame in which it dispenses water, e.g. it will do this at any time of the day, whenever the moisture gets too low.

### How to interpret sensor readings

Each sensor produces a numerical value between 0 and 4095 representing the soil moisture level of its surrondings. The used sensor do not measure an absolute value, two different sensors may produce different readings. Also repositing the sensor typically changes the reading for the same moisture level. 

If the sensor reads lower values, the soil is more moist. Values of around 3300 - 3400 indicate extrem dryness, values around 1600 extrem wetness, or even mean that the sensor is sitting in water. A lot of factors influence the actual readings, so take the values mentioned above as guidance but be prepared to see significant differences. 


## How to setup 

After initial software testing (see below), put together the hardware. Be prepared for water to flow out fron unexpectd places once running with water.

1. Test pump
* Water Intake: The water intake tube can be long if using the recommended 12 V pump, place this in the water reservoir and secure tube from sliping out, e.g. with a clamp.
* Use tube from pump outlet to water reservoir or a different cup / bowl of appropriate size.
* Start water flow by  sending `'{ "dose": 100, "outlet": 1  }` to mqtt topic `plant32/in/1` 
* Pump should start to suck water from reservoir. After a while the pump should stop and report a pump issue (as we have been testing without flow sensor, system is not able to know if and how much water has been flowing)
2. Test flow sensor
* Now connect flow sensor with tubes. You may connect flow sensor before pump intake or after pump outlet. Run pump again (requires reset). Water should flow and stop as before. 
* Now connect sensor to board. Keep an eye on the correct wiring, wrong wiring will destroy the sensor and/or the ESP32 processor. Run same test as before. This time no pump issue should be reported and water amount in ml should be approximately matching the given in the messsage as dose.
3. Test valves
* Connect valves via tubes and water flow splitters to the outlet of the pump or water flow sensor (if it is placed after pump outlet). Put tubes from valve to approriate cup/bowl to collect dispensed water. It is a good idea to start with just one valve connected. Remember to make sure that the water has no escape. 
* Now run command above, water should flow as before and go through a single valve. Changing the target number should open a different valve.

## How to interact with the device

### Setting up a plant in a pot

1. Water the plant to a desired level maximum level of soil moisture.
3. Place the sensor in its final location. Keep in mind, it should be out of the way of the water of the irrigation tube. 
2. Let the plant sit for at least an hour to have the water throughly spread in the pot and achieve even soil moisture.
4. Read measurement via MQTT or serial. Make sure to pick the right sensor value ...
5. Set the limit value for this outlet via MQTT: `{ "set": {  "property": "l", "value": 3000, "target": 1 }}` to the in topic of the device, e.g. "plant32/in/1". It should respond with "CMD_OK" on the event topic.
6. Finally decide about the amount of water dispenses (the "dose") in a single pumping action. Start with small values, e.g. 30 ml, and raise if too many watering actions are required to achieve desired moisture level. To set the dose, send via MQTT: `{ "set": { "property": "d", "value": 30, "target": 1 }}` to the in topic of the device, e.g. "plant32/in/1". It should respond with "CMD_OK" on the event topic. 


## How to configure / Reference of MQTT available commands

At first start all outlets are configured to never dispense water unless external instructed to do so. That is achieved by setting the moisture level to 4096, which the sensor will never read *AND* by setting the dose to 0. Any changes to these values via MQTT are stored permantely in the device and dumped at startup via MQTT and serial.

Each device must have a unique set of topics. This is normally achieved by setting a different MY_MQTT_INSTANCE value during compilation. In this reference we assume this id to be `1`.  The prefix (by default `plant32`) can be changed during compilation in the same way. Hence the default topics are `plant32/in/1`, `plant32/out/1`, and `plant32/event/1`.

All commands have to be sent to the IN topic of the device, e.g. "plant32/in/1". Watch serial output at startup and make sure to use the topic shown there. Each command is acknowledged on the EVENT topic (e.g. "plant32/event/1"). Normal operational data is communicated via the OUT topic.

1. Moisture shall be kept below 3000 for outlet 1: send  `{ "set": { "property": "l",  "value": 3000, "target": 1 }}`  
2. Water in a single pump action should be 30ml for outlet 1: send  `{ "set": { "property": "d": ,  "value": 30, "target": 1 }}`
3. Reset an reported pump issue: send  `{ "reset":  true }`
4. Trigger a measurement immediately: send  `{ "measure":  true }`
5. Have the device report the internal time: send  `{ "time":  true }`
6. Get extra water shot of 100ml for outlet 3: send  `{ "dose": 100, "outlet": 3 }`


## How to build software

Software is based on the Arduino framework in combination with platformio. The software is not intended to be used with the ArduinoIDE. It is written in modern C++. 

You will also need access to a mqtt broker or run one. For instances `mosquitto` is a fine choice.
Instructions on setting up a simple MQTT broker can be found on the internet, e.g. http://www.steves-internet-guide.com/mosquitto-broker/ 


### Recommended Approach for testing the build and setup:

Using a ESP32 DevkitC or similar device without being plugged into the ESP32 WaterControl board is recommended here as it minimizes the risk of things going wrong.


1. Setup compilation environment

    * Watch a tutorial on VS Code and Platformio
    * Install Visual Studio Code
    * Inside VS Code, install the platformio extension.
    * Either clone project from Gitlab directly inside VS Code or externally
    * Open the project folder with VS Code
    * Copy lib/MqttBase/src/mqtt_base_config.h.template to lib/MqttBase/src/mqtt_base_config.h and add your Wifi
      credentials and the information for the mqtt server to this file.
    * Start the build for one of the two devices esp32dev_dev1 or esp32dev_dev2
    * Connect an ESP32 DevkitC (or compatible device) and flash software using "Upload and Monitor".


    Output in the serial monitor should look similar to this:

    ```    
    ESP8266/ESP32 MQTT enabled device
    Firmware Version: heads/master-0-ge7b6280
    Reboot Reason: 7

    Connecting to 'YOUR_WIFI_NETWORK' as host 'plant32-XXXX'.....
    WiFi connected, IP address: 192.168.XXX.XXX
    Setting NTP server/ saving poweron time
    Date: 05.06.21  Zeit: 16:57:24
    Before MQTT (re)connect

    Last MQTT reconnect was 8 seconds ago
    Attempting MQTT connection...connected!
    Output / Event topics : plant32/out/1 / plant32/event/1
    Subscribed to plant32/in/1
    [{"outlet_id":1,"adcPin":36,"limit":4096,"dose":10,"outlet":{"active":false,"valvePin":12,"pumpPin":18}},{"outlet_id":2,"adcPin":39,"limit":4096,"dose":0,"outlet":{"active":false,"valvePin":14,"pumpPin":18}},{"outlet_id":3,"adcPin":34,"limit":4096,"dose":0,"outlet":{"active":false,"valvePin":27,"pumpPin":18}},{"outlet_id":4,"adcPin":35,"limit":4096,"dose":0,"outlet":{"active":false,"valvePin":26,"pumpPin":18}},{"outlet_id":5,"adcPin":32,"limit":4096,"dose":0,"outlet":{"active":false,"valvePin":25,"pumpPin":18}},{"outlet_id":6,"adcPin":33,"limit":4096,"dose":0,"outlet":{"active":false,"valvePin":13,"pumpPin":18}}]
    {"ports":[{"v":3348,"s":0},{"v":3472,"s":0},{"v":144,"s":0},{"v":0,"s":0},{"v":553,"s":0},{"v":258,"s":0}]}
    ```


2. Test MQTT monitoring

    You should subscribe with a mqtt tool like mosquitto_sub to the MQTT topic "water/#" e.g. `mosquitto_sub -h <my_mqtthost> -v -t "water/#" 

    Reset the Device, and you should see MQTT messages coming in, this should look similar to this:

    ```
    plant32/event/1 READY plant32-6000 reconnects:1/ reboots: 0
    plant32/event/1 [{"outlet_id":1,"adcPin":36,"limit":4096,"dose":10,"outlet":{"active":false,"valvePin":12,"pumpPin":18}},{"outlet_id":2,"adcPin":39,"limit":4096,"dose":0,"outlet":{"active":false,"valvePin":14,"pumpPin":18}},{"outlet_id":3,"adcPin":34,"limit":4096,"dose":0,"outlet":{"active":false,"valvePin":27,"pumpPin":18}},{"outlet_id":4,"adcPin":35,"limit":4096,"dose":0,"outlet":{"active":false,"valvePin":26,"pumpPin":18}},{"outlet_id":5,"adcPin":32,"limit":4096,"dose":0,"outlet":{"active":false,"valvePin":25,"pumpPin":18}},{"outlet_id":6,"adcPin":33,"limit":4096,"dose":0,"outlet":{"active":false,"valvePin":13,"pumpPin":18}}]
    plant32/out/1 {"ports":[{"v":3348,"s":0},{"v":3472,"s":0},{"v":144,"s":0},{"v":0,"s":0},{"v":553,"s":0},{"v":258,"s":0}]}
    ```

    It dumps its internal hardware configuration at startup and also its hostname/ip in the wifi network. 


3. Test  MQTT control

    Use a mqtt client to publish a JSON formatted message to the input topic "water/in/1", e.g. 
    `mosquitto_pub -h <my_mqtthost> -t "water/in/1" '{ "dose": 30, "outlet": 1  }'` 

    The device should now respond with some MQTT messages. As you should not test it in the final setup with water, the system will detect a pump issue and report accordingly.
    Further pump operations will be blocked until the board is reset either by pressing the boot button or the reset button physically or sending an MQTT reset message `'{ "reset": true }'`

### Configuring the software

Most of the software configuration is build time configuration through #define parameters.

### 
### Preprocessor Defines

* WIFI: MY_WIFI_SSID, MY_WIFI_KEY (in mqtt_base_config.h, not to be stored in git!)
* MQTT: MY_MQTT_PREFIX, MY_MQTT_INSTANCE, MY_MQTT_IN_TOPIC, MY_MQTT_OUT_TOPIC, MY_MQTT_EVT_TOPIC (in mqtt_base.cpp), MY_MQTT_SERVER_IP (in mqtt_base_config.h)

### Configuration only available through code changes

#### main.cpp
* Hardware pin layout (sensor pins, pump pins, rotary encoder pins, button pins, valve pins), coupling of sensor and outlet( pump, valve)
* Timings for measurement interval, watering interval, stop after no change time, number of waterings until no change, 

