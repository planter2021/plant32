# MQTT BASE Library

Provides a simple approach for "high availability" mqtt controlled applications on ESP32 and ESP8266.
It makes creating a simple MQTT-enabled application as easy as possible.

It has an input, an output and an event mqtt topic. 

Input messages are passed to the application, output topic is used to provide normal operational information to the MQTT broker, the event topic is used to report lifecycle events (such as becoming ready after a (re)start). 

It also handles reliable unique hostname generation, even if multiple host have the same binary flashed. 

It makes sure the device stays connected to WiFi and MQTT. As last resort it restarts the ESP to try to get connectivity. It also activates the watchdog.



```ini
[env]
...
extra_scripts = 
    pre:lib/MqttBase/auto_firmware_version.py

```

```C++
#include <Arduino.h>

void setup(){
    serial.begin(115200);
    serial.print("Firmware Version: ");
    serial.println(AUTO_VERSION);   // Use the preprocessor directive

   // OR //

   char firmware_char_array[] = AUTO_VERSION;
   serial.println(firmware_char_array, sizeof(firmware_char_array));
}

void loop(){
    // Loop
}
```
