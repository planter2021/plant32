/*
    This file is part of the Plant32 ESP32 Plant Water Project.

    Plant32 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Plant32 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Plant32.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Arduino.h>
#include "mqtt_base.h"

#ifdef ESP8266
#include <ESP8266WiFi.h>
#include <time.h>
#endif

#include <PubSubClient.h>

#include "mqtt_base_config.h"

#ifdef ESP32
#include <WiFi.h>
#include <esp_pm.h>
#include <esp_wifi.h>
#include <esp_wifi_types.h>
#include <esp_task_wdt.h>
#endif

#include <stdlib.h>

#define MQTT_LOST_RESTART_WAIT (3600*1000) // wait time in ms before we try a restart to fix lost mqtt server connection

#ifndef MY_MQTT_INSTANCE
#define MY_MQTT_INSTANCE (unique_idstr) 
#endif

#ifndef MY_MQTT_PREFIX
#define MY_MQTT_PREFIX "demo"
#endif

#ifndef MY_MQTT_EVT_TOPIC
#define MY_MQTT_EVT_TOPIC (String(MY_MQTT_PREFIX) + "/event/" + String(MY_MQTT_INSTANCE))
#endif
#ifndef MY_MQTT_OUT_TOPIC
#define MY_MQTT_OUT_TOPIC (String(MY_MQTT_PREFIX) + "/out/" + String(MY_MQTT_INSTANCE))
#endif
#ifndef MY_MQTT_IN_TOPIC
#define MY_MQTT_IN_TOPIC (String(MY_MQTT_PREFIX) + "/in/" + String(MY_MQTT_INSTANCE))
#endif

uint64_t getUniqueMachineId()
{
#ifdef ESP32
  return ESP.getEfuseMac();
#else
  return ESP.getChipId();
#endif
}

const char *ssid = MY_WIFI_SSID;
const char *password = MY_WIFI_KEY;

String unique_idstr((uint32_t)getUniqueMachineId() % 10000);


const char *mqtt_server = MY_MQTT_SERVER_IP;

String mqtt_evt_topic(MY_MQTT_EVT_TOPIC);
String mqtt_in_topic(MY_MQTT_IN_TOPIC);
String mqtt_out_topic(MY_MQTT_OUT_TOPIC);

String getUniqueHostname(const char *prefix)
{
  char hostname[64];
  snprintf(hostname, sizeof(hostname), "%s-%04" PRIu64, prefix, getUniqueMachineId() % 10000);
  return hostname;
}

#ifndef MY_HOST_PREFIX
#ifdef ESP32
#define MY_HOST_PREFIX "esp32-mqtt"
#else
#define MY_HOST_PREFIX "esp-mqtt"
#endif
#endif

const String my_hostname = getUniqueHostname(MY_HOST_PREFIX);

void connect_wifi(bool wait_until_connected = true)
{
  Serial.println();
  Serial.printf("Connecting to '%s' as host '%s'", ssid, my_hostname.c_str());

  while (WiFi.status() != WL_CONNECTED)
  {
    WiFi.begin((char *)ssid, password);
    // WiFi.setTxPower(WIFI_POWER_19_5dBm);
    for (int loops = 10; loops > 0 && WiFi.status() != WL_CONNECTED; loops--)
    {
      delay(500);
      Serial.print(".");
    }
    if (WiFi.status() != WL_CONNECTED)
    {
      // sometimes a restart helps to get connected
      //ESP.restart();
      Serial.print("!");
      WiFi.disconnect();
      WiFi.mode(WIFI_OFF); // if these don't compile, you have the wrong WiFi lib included!
      WiFi.mode(WIFI_STA);

      delay(1000);
    }
    if (wait_until_connected == false)
    {
      break;
    }
  }

  Serial.println();
  Serial.print("WiFi connected, IP address: ");
  Serial.println(WiFi.localIP());
}

void setup_wifi()
{

  delay(10);
// We start by connecting to a WiFi network
#ifdef ESP32
  WiFi.setHostname(my_hostname.c_str());
#else
  WiFi.hostname(my_hostname.c_str());
#endif
  connect_wifi(true);
  randomSeed(micros());
}

WiFiClient espClient;
PubSubClient client(espClient);

unsigned long last_reconnect;
long num_reconnect;

#ifdef ESP8266
#define _SECTION_ATTR_IMPL(SECTION, COUNTER) __attribute__((section(SECTION "." _COUNTER_STRINGIFY(COUNTER))))

#define _COUNTER_STRINGIFY(COUNTER) #COUNTER

// Forces to put some user defined data in the binary file header, the offset is 0x10.
#define USER_DATA_ATTR _SECTION_ATTR_IMPL(".user.data", __COUNTER__)

// Forces data into noinit section to avoid initialization after restart.
#define __NOINIT_ATTR _SECTION_ATTR_IMPL(".noinit", __COUNTER__)

// Forces data into RTC slow memory of .noinit section.
// Any variable marked with this attribute will keep its value
// after restart or during a deep sleep / wake cycle.
#define RTC_NOINIT_ATTR _SECTION_ATTR_IMPL(".rtc_noinit", __COUNTER__)
#endif

RTC_NOINIT_ATTR int num_reboot;

bool reconnect()
{
  Serial.println();
  Serial.println(String("Last MQTT reconnect was ") + String((millis() - last_reconnect) / 1000) + " seconds ago");
  // Loop until we're reconnected
  for (int count = 10; count > 0 && (!client.connected()); count--)
  {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str()))
    {
      Serial.println("connected!");
      Serial.printf("Output / Event topics : %s / %s\n",mqtt_out_topic.c_str(), mqtt_evt_topic.c_str());

      if (client.subscribe(mqtt_in_topic.c_str()))
      {
        Serial.printf("Subscribed to %s\n",mqtt_in_topic.c_str());
      }
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }

  last_reconnect = millis();
  return client.connected();
}

#ifdef ESP32
typedef esp_reset_reason_t RESET_REASON_T;
#define RESET_POWERON ESP_RST_POWERON
#else
typedef uint32_t RESET_REASON_T;
#define RESET_POWERON REASON_DEFAULT_RST
#endif

void wdt_feed()
{
#ifdef ESP32
  esp_task_wdt_reset(); // feed the dog...
#else
  ESP.wdtFeed();
#endif
}
void wdt_init()
{
#ifdef ESP32
  esp_task_wdt_init(60, true);
  esp_task_wdt_add(NULL);
  esp_task_wdt_reset();
#else
  ESP.wdtEnable(60 * 1000);
#endif
}

void callback(char *topic, byte *payload, unsigned int length)
{
  /*
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  */

  char msg[length + 1];
  strncpy(msg, (char *)payload, length);
  msg[length] = '\0';

  mqtt_process_in(msg);
}

RESET_REASON_T getResetReason()
{
  RESET_REASON_T retval;
#ifdef ESP32
  retval = esp_reset_reason();
#else
  rst_info *resetInfo;
  resetInfo = ESP.getResetInfoPtr();
  retval = (*resetInfo).reason;
#endif
  return retval;
}
void count_num_reboot()
{
  RESET_REASON_T reason = getResetReason();
  Serial.printf("Reboot Reason: %d\n", (int)reason);
  if ((reason == RESET_POWERON))
  {
    num_reboot = 0;
  }
  else
  {
    num_reboot++;
  }
  if (num_reboot > 0)
  {
    Serial.printf("Reboots after Power On: %d", num_reboot);
  }
}

// RTC
#define NTP_SERVER "pool.ntp.org"
//#define NTP_SERVER "212.227.158.163"
#define TZ_INFO "CET-1CEST,M3.5.0,M10.5.0/03:00:00"

tm poweron_time;
void run_ntp()
{
  Serial.println("Setting NTP server/ saving poweron time");
  configTzTime(TZ_INFO, NTP_SERVER); // ESP32 Systemzeit mit NTP Synchronisieren
  //configTime("", NTP_SERVER, nullptr, (const char*)nullptr);
  //getLocalTime(&poweron_time, 10000);      // Versuche 10 s zu Synchronisieren
  for (int i = 10; !time(nullptr) && i > 0; i--)
  {
    delay(1000);
  }
}

void setup_timezone()
{
#ifdef ESP32
  setenv("TZ", TZ_INFO, 1); // Zeitzone  muss nach dem reset neu eingestellt werden
  tzset();
#endif
}

bool has_good_time()
{
  time_t now;
  struct tm info;
  time(&now);
  localtime_r(&now, &info);
  return (info.tm_year > (2016 - 1900));
}

#include <time.h>

bool setup_rtc()
{
  bool retval = false;
  bool is_70 = true;

#ifdef ESP8266
  time_t rawtime;
  time(&rawtime);

  is_70 = rawtime < 3600 * 24 * 365;
#else
  tm local;
  getLocalTime(&local);
  is_70 = local.tm_year == 70;
#endif
  if (getResetReason() == RESET_POWERON || is_70)
  {
    run_ntp();
    retval = true;
  }
  return retval;
}

void print_time()
{
#ifdef ESP32
  tm local;
  getLocalTime(&local);
  Serial.println(&local, "Date: %d.%m.%y  Zeit: %H:%M:%S");
#else
  time_t now = time(nullptr);
  Serial.println(ctime(&now));
#endif
}

bool lost_connection = false;
waitTimer<MQTT_LOST_RESTART_WAIT> lost_connection_time;

void mqtt_loop()
{
  wdt_feed();
  
  if (!client.connected())
  {
    Serial.println("Before MQTT (re)connect");
    num_reconnect++;

    // make sure we have WiFi working
    if (WiFi.status() != WL_CONNECTED)
    {
      Serial.println("WiFi not connected, trying WiFi reconnect");
      connect_wifi(false);
    }

    if (WiFi.status() == WL_CONNECTED)
    {
      if (reconnect() == false)
      {
        // we wait before we try a restart
        if (lost_connection && lost_connection_time.check() )
        {
          Serial.println("Restarting ESP after failed reconnect");
          ESP.restart();
        }
        else
        {
          lost_connection = true;
          lost_connection_time.start();
        }
      }
      else
      {
        lost_connection = false;
        //setenv("TZ", TZ_INFO, 1);
        //time_t now __attribute__((unused)) = time(nullptr);
        mqtt_event((String("READY ") + my_hostname + " reconnects:" + num_reconnect + "/ reboots:" + num_reboot ).c_str());
      }
    }
  }
  client.loop();

  static waitTimer<60*1000> ntpThrottle;

  if (has_good_time() != true && ntpThrottle.check_restart())
  {
    run_ntp();
  }
}

void mqtt_event(const char *msg)
{
  client.publish(mqtt_evt_topic.c_str(), msg);
}

void mqtt_out(JsonDocument& doc)
{
  char msg[MQTT_MAX_PACKET_SIZE];
  serializeJson(doc, msg);
  mqtt_out(msg);
}

void mqtt_event(JsonDocument& doc)
{
  char msg[MQTT_MAX_PACKET_SIZE];
  serializeJson(doc, msg);
  mqtt_event(msg);
}

void mqtt_out(const char *msg)
{
  client.publish(mqtt_out_topic.c_str(), msg);
}

void mqtt_setup()
{
#define ENABLE_MQTT
  Serial.println("ESP8266/ESP32 MQTT enabled device");

#ifdef AUTO_VERSION  
  Serial.print("Firmware Version: ");
  Serial.println(AUTO_VERSION);   // Use the preprocessor directive
#endif

#ifdef ENABLE_MQTT
  setup_timezone();
  count_num_reboot();

#ifdef HAS_DISPLAY
  I2Caccess = xSemaphoreCreateMutex(); // for access management of i2c bus
  if (I2Caccess)
  {
    xSemaphoreGive(I2Caccess); // Flag the i2c bus available for use
  }

  init_display("Solar2MQTT", "1.0");
#endif

  wdt_init();

  setup_wifi();
  setup_rtc();
  print_time();
  Serial.flush();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

#ifdef ESP32
  esp_wifi_set_ps(WIFI_PS_MIN_MODEM);
#endif
#endif
}