/*
    This file is part of the Plant32 ESP32 Plant Water Project.

    Plant32 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Plant32 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Plant32.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <inttypes.h>
#include <ArduinoJson.h>

void mqtt_setup();
void mqtt_loop();
void mqtt_event(const char* msg);
void mqtt_out(const char* msg);

void mqtt_out(JsonDocument& doc);
void mqtt_event(JsonDocument& doc);

void mqtt_process_in(const char* msg);
bool setup_rtc(); // returns true if ntp setup for RTC has been called, false otherwise. Note: true does not indicate success
bool has_good_time();
void print_time();

template <const uint32_t D> class waitTimer
{
  uint32_t when = UINT32_MAX-D;
  public:
    waitTimer() {}
    waitTimer(uint32_t _when): when(_when) {} 
    IRAM_ATTR bool check(uint32_t now) const { return (now - when)> D; }
    IRAM_ATTR bool check()  { return check(millis()); }
    IRAM_ATTR void start(uint32_t now) { when = now;}
    IRAM_ATTR void start() { start(millis()); }
    IRAM_ATTR bool check_restart(uint32_t now) { bool retval = check(now); if(retval) { start(now); } ; return retval; }
    IRAM_ATTR bool check_restart()  { return check_restart(millis()); }
    IRAM_ATTR bool is_active(uint32_t now) { return (now - when) <= D; }
    IRAM_ATTR bool is_active() { return is_active(millis()); }
    IRAM_ATTR uint32_t get() const { return when; }
};
