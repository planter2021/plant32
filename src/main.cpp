/*
    This file is part of the Plant32 ESP32 Plant Water Project.

    Plant32 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Plant32 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Plant32.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <array>
#include <Arduino.h>
#include <ArduinoJson.h>

#include "mqtt_base.h"
#include "EEPROM.h"

#include <SPI.h>
#include <Wire.h>

// #define TEST_MODE
// CONFIGURATION PARAMETERS
#ifndef TEST_MODE
const uint32_t waitAfterWatering = 70 * 60;   // unit: seconds
const uint32_t waitNextMeasurement = 10 * 60; // unit: seconds // should be significantly shorter than waitAfterWatering
const uint32_t waitAfterNoChange = 12 * 3600; // unit: seconds, 12h
const uint32_t pumpRepeats = 6;
#else
const uint32_t waitAfterWatering = 20;  // unit: seconds
const uint32_t waitNextMeasurement = 2; // unit: seconds // should be significantly shorter than waitAfterWatering
const uint32_t waitAfterNoChange = 30;  // unit: seconds
const uint32_t pumpRepeats = 4;
#endif

const uint32_t defaultDose = 0;         // 30 ml
const uint32_t defaultThreshold = 4096; // off

// how many milliseconds are required to pump 1ml? Use upper estimate based on measurements as this is used to calculate the timeout, actual flow is measure
// if changing, start with a large number and do measurements, then reduce to calculated value plus 5% margin.
const uint32_t pump_time_per_ml = 150; 

// how many milliseconds are considered startup time to get started from a completely empty system. 
// Measure by starting let the system pump air (simulate empty reservoir). Now dispense a larger amount of water (e.g. 200ml) and record timing
// Now dispense same amout of water again, take difference, add 10% margin and use here as startup time.
const uint32_t pump_time_startup = 2000; 


#define LED_WARN 16
#define ENC_SW 17
#define BUTTON_RESET 0


#ifdef ESP32_ECO_POWER
const int16_t forbidden_pins = { 23, 19, 18, 5, 16, 17, 35 };
#endif


const int16_t sensorPins[] = {A0, A3, 34, 35, 32, 33};
// V1.0
// const int16_t valvePins[] = {12, 14, 27, 26, 25, 13};
//  V1.1
const int16_t valvePins[] = {13, 12, 14, 27, 26, 25};
const int16_t pumpPins[] = {18, 19};
const int16_t flowmeterPins[] = {5};

/*
ESP32 Water v1.0
Label / Pin
J1/36 -> J10/12
J2/39 -> J9/14
J3/34 -> J8/27
J4/35 -> J7/26
J5/32 -> J12/25
J6/32 -> J11/13
*/
/*
ESP32 Water v1.1
Label / Pin
S1/36 -> V1/13
S2/39 -> V2/12
S3/34 -> V3/14
S4/35 -> V4/27
S5/32 -> V5/26
S6/32 -> V6/25
*/
// FlowMeter parameter, see datasheet of flow meter
const double flowStepMl = 0.5; // each flowmeter pluse represents flowStepMl milliliters

// CODE, NO NEED TO MODIFY FOR CONFIGURATION PURPOSES BELOW
void serial_and_mqtt_event(const char *msg)
{
  mqtt_event(msg);
  Serial.println(msg);
}

void serial_and_mqtt_event(JsonDocument& doc)
{
  mqtt_event(doc);
  serializeJson(doc,Serial);
  Serial.println();
}

void serial_and_mqtt_out(JsonDocument& doc)
{
  mqtt_out(doc);
  serializeJson(doc,Serial);
  Serial.println();
}


template<const uint16_t PIN, const bool inverseLogic = false>
class OutputPin
{
public:
  OutputPin() { pinMode(PIN, OUTPUT); }
  void operator()(bool on)
  {
    if (inverseLogic){
      digitalWrite(PIN, on?LOW:HIGH);
    }
    else
    {
      digitalWrite(PIN, on?HIGH:LOW);
    }
  }
};

OutputPin<LED_WARN, true> led_warn;

class StepperControl
{
  const int dir_pin;
  const int step_pin;
  const int en_pin;

public:
  StepperControl(const int dir_p, const int step_p, const int en_p) : dir_pin(dir_p), step_pin(step_p), en_pin(en_p)
  {
  }

  void step(const uint16_t duration)
  {
    digitalWrite(step_pin, HIGH);
    delayMicroseconds(5);
    digitalWrite(step_pin, LOW);
    delayMicroseconds(duration - 5);
  }

  void steps(uint16_t number)
  {
    digitalWrite(dir_pin, HIGH);
    digitalWrite(en_pin, LOW);
    for (; number; number--)
    {
      step(1000);
    }
    digitalWrite(en_pin, HIGH);
  }

  void setup()
  {
    pinMode(step_pin, OUTPUT);
    pinMode(dir_pin, OUTPUT);
    pinMode(en_pin, OUTPUT);
    digitalWrite(en_pin, HIGH);
  }
};

class RelayControl
{
  const int en_pin;
  const bool open_high;

public:
  RelayControl(const int en_p, bool open_high_p) : en_pin(en_p), open_high(open_high_p)
  {
    setup();
  }

  void open()
  {
    digitalWrite(en_pin, open_high ? HIGH : LOW);
  }
  void close()
  {
    digitalWrite(en_pin, open_high ? LOW : HIGH);
  }

  void setup()
  {
    pinMode(en_pin, OUTPUT);
    close();
  }

  int pin() const
  {
    return en_pin;
  }
};

class FlowMeter
{
  const int pulse_pin;
  const double ml_per_pulse;
  uint32_t counter; // at 0.5ml per pulse we can record roughly 4*10^6 liters
  bool counted;

  waitTimer<10> last_wf;

public:
  int16_t isr_pin() { return pulse_pin; }
  int16_t isr_mode() { return RISING; }

  IRAM_ATTR void isr() 
  {
    if (last_wf.check_restart())
    {
      counter++;
      counted = true;
    }
  }

  FlowMeter(int16_t pin, double mlpp) : pulse_pin(pin), ml_per_pulse(mlpp) { setup(); }

  bool new_count()
  {
    bool c = counted;
    counted = false;
    return c;
  }
  uint32_t pulses() { return counter; }
  uint32_t last_pulse() { return last_wf.get(); }
  double ml() { return counter * ml_per_pulse; }
  void setup()
  {
    pinMode(pulse_pin, INPUT_PULLUP);
  }
};

class PumpControl
{
  bool issue = false;
  bool active = false;
  uint32_t pump_start_t = 0;
  uint32_t pump_stop_t = 0;
  uint32_t pump_timeout = 0;
  uint32_t pump_start_vol = 0; 

  RelayControl &pump;
  FlowMeter &fm;

  uint32_t target = 0;
  uint32_t amount = 0;

public:
  PumpControl(RelayControl &p, FlowMeter &f) : pump(p), fm(f)
  {
  }

  void set_issue() { issue = true; }
  bool has_issue() { return issue; }

  // amount to pump in ml
  void setDose(uint32_t _amount)
  {
    amount = _amount;
    target = fm.ml() + amount;
  }

  bool doseDelivered()
  {
    return fm.ml() >= target;
  }

  bool is_active()
  {
    return active;
  }

  void process()
  {
    uint32_t now = millis();

    bool pump_on = doseDelivered() == false && issue == false;
    if (pump_on != active)
    {
      if (pump_on == false)
      {
        pump_stop_t = now;
        uint32_t pump_stop_vol = fm.ml();

        StaticJsonDocument<200> doc;
        auto obj = doc.to<JsonObject>();
        obj["pumpTime"] = pump_stop_t - pump_start_t;
        obj["pumpVolume"] = pump_stop_vol - pump_start_vol;
        obj["timeLimit"] = pump_timeout;
        obj["pumpIssue"] = issue;

        serial_and_mqtt_event(doc);
        active = false;
        pump.close();
        target = fm.ml();
      }
      else
      {
        pump_start_t = now;
        pump_start_vol = fm.ml();
        active = true;
        pump.open();
        pump_timeout = pump_time_startup + (pump_time_per_ml * amount); 
      }
    }

    if (issue == false && active == true && (now - pump_timeout) > pump_start_t)
    {
      // HACK
#ifndef TEST_MODE
      // issue = true;
      target = fm.ml();
#else
      pump.close();
      reset();
#endif
      serial_and_mqtt_event("PUMP_ISSUE");
    }
  }

  void reset()
  {
    serial_and_mqtt_event("PUMP_ISSUE_RESET");
    issue = false;
    active = false;
    target = fm.ml(); // set target to currently delivered amount.
  }

  void print()
  {
    Serial.printf("{ \"active\": %d, \"pin\": %d }\n", active, pump.pin());
  }

  int pin() const
  {
    return pump.pin();
  }
};

class WaterOutlet
{
  PumpControl &pump;
  RelayControl &valve;

  uint32_t doseTarget = 0;
  bool active = false;

public:
  WaterOutlet(PumpControl &p, RelayControl &v) : pump(p), valve(v)
  {
  }

  void doseStart(uint32_t amount)
  {
    doseTarget = amount;
  }

  void process()
  {
    if (is_active() == false && doseTarget > 0)
    {
      if (pump.has_issue() == false && pump.is_active() == false && pump.doseDelivered())
      {
        pump.setDose(doseTarget);
        Serial.printf("setDose %d\n", doseTarget);
        doseTarget = 0;
        valve.open();
        active = true;
      }
    }

    if (is_active() && (pump.doseDelivered() || pump.has_issue()))
    {
      valve.close();
      active = false;
    }
  }

  bool is_active() { return active; }

  void print(JsonObject &obj)
  {
    obj["active"] = active;
    obj["valvePin"] = valve.pin();
    obj["pumpPin"] = pump.pin();
  }

  bool has_issue()
  {
    return pump.has_issue();
  }

  void reset()
  {
    doseTarget = 0;
    valve.close();
    active = false;
  }
};

extern FlowMeter fm;

IRAM_ATTR void fm_isr()
{
  fm.isr();
}

template <typename T, const int SIZE = 16>
class AverageBuffer
{
  T avg;
  T total;
  int count;
  bool valid;

public:
  AverageBuffer()
  {
    reset();
  }
  void reset()
  {
    count = SIZE;
    valid = false;
  }
  void append(const T value)
  {
    if (count == 0)
    {
      // Serial.println("C1");
      total += (value - avg);
      avg = total / SIZE;
    }
    else if (count != SIZE)
    {
      // Serial.println("C2");
      count--;
      total += value;
      avg = total / (SIZE - count);
    }
    else // first element
    {
      // Serial.println("C3");
      avg = total = value;
      count--;
    }
  }
  const T getAverage()
  {
    return avg;
  }
  /**
   * @brief Returns true if average is calculated from full number of values 
   * 
   * @return true average is calculated from SIZE elements
   * @return false average is calculated from less than SIZE elements
   */
  bool hasAverage()
  {
    return count == 0;
  }
};

extern std::vector<WaterOutlet> outlets;

struct WaterPortStoredData
{
  int16_t limit;
  uint16_t dose;
  uint16_t outletid;
};

// if we have pumped multiple times without reaching desired moisture level, we stop trying for some time to avoid flooding
// accidentially
waitTimer<waitAfterNoChange * 1000> stopTimer;

template <class Outlet>
class WaterPort
{
public:
  int port;
  WaterPortStoredData wpsd;
  uint16_t value;
  uint32_t amount = 0;
  uint16_t waterLowCount;
  int16_t old_adc;
  Outlet *outlet_ptr;
  uint16_t pumpCount = 0;

  uint8_t outlet_id;

  AverageBuffer<int, 4> avg;
  WaterPort(const int p, const uint8_t _outlet_id) : port(p), waterLowCount(0), outlet_ptr(NULL), outlet_id(_outlet_id)
  {
  }

  void setup()
  {
    recall();
    if (wpsd.dose == 0xffff || wpsd.dose == 0)
    {
      wpsd.dose = defaultDose;
    }
    if (wpsd.limit == -1 || wpsd.limit == 0 /* 0xffff */)
    {
      wpsd.limit = defaultThreshold;
    }
    if (wpsd.outletid == 0xffff || wpsd.outletid == 0)
    {
      wpsd.outletid = outlet_id;
    }
    setOutlet(wpsd.outletid);
  }

  void recall() { EEPROM.get(port * sizeof(WaterPortStoredData), wpsd); }
  void store() { EEPROM.put(port * sizeof(WaterPortStoredData), wpsd); }
  void setLimit(uint16_t l)
  {
    wpsd.limit = l;
    store();
  }
  void setDose(uint16_t d)
  {
    wpsd.dose = d;
    store();
  }
  void setOutlet(uint8_t pid)
  {
    wpsd.outletid = pid;
    outlet_ptr = (pid > 0 && pid <= outlets.size()) ? &outlets[pid - 1] : NULL;
    store();
  }

  void print(JsonObject &obj)
  {
    obj["adcPin"] = port;
    obj["limit"] = wpsd.limit;
    obj["dose"] = wpsd.dose;
    obj["outlet"] = wpsd.outletid;

    JsonObject obj2 = obj.createNestedObject("outlet");

    if (outlet_ptr != NULL)
    {
      outlet_ptr->print(obj2);
    }
  }
  uint32_t getAmount() { return amount; }
  uint32_t resetAmount()
  {
    uint32_t lamount = amount;
    amount = 0;
    return lamount;
  }

  void process()
  {
    if (outlet_ptr != NULL)
    {
      if (needsWater() && pumpCount < pumpRepeats && outlet_ptr->has_issue() == false)
      {
        pumpCount++;
        amount += wpsd.dose;
        outlet_ptr->doseStart(wpsd.dose);
        waterLowCount = 1;
        {
          StaticJsonDocument<200> doc;
          auto obj = doc.to<JsonObject>();
          obj["port"] = port;
          obj["dose"] = wpsd.dose;
          obj["amount"] = amount;
          obj["pump_count"] = pumpCount;
          
          serial_and_mqtt_out(doc);
        }
      }

      // we wait for some hours after a number of pump actions without reaching the . Probably no water in tank or broken tube etc.
      if (pumpCount == pumpRepeats)
      {
        if (stopTimer.check())
        {
          stopTimer.start();
          Serial.println("Pump Stop Timer started");
        }
        pumpCount++;
      }
      if (pumpCount == pumpRepeats + 1)
      {
        if (stopTimer.check())
        {
          Serial.println("Pump Stop Timer finished");
          pumpCount = 0;
        }
      }
    }
    // wait some minutes
  }

  void update()
  {
    avg.append(map(analogRead(port), 0, 5000, 0, 5000));
    if (avg.getAverage() > wpsd.limit)
    {
      waterLowCount++;
    }
    else
    {
      pumpCount = 0;
      waterLowCount = 0;
    }
  }

  bool needsWater()
  {
    return waterLowCount > 4;
  }

  void printAdc(int adc, bool force = false)
  {
    old_adc = -1;
    if (old_adc != adc || force == true)
    {
      Serial.print(F("s -> Value :"));
      Serial.print(adc);
      Serial.print(F(" ( 3.3V: "));
      Serial.print((adc * 50) / 33);
      Serial.println(F(")"));
    }
    old_adc = adc;
  }
};

FlowMeter fm(flowmeterPins[0], flowStepMl);

std::vector<RelayControl> valves;
std::vector<RelayControl> pump_switches;
std::vector<PumpControl> pumps;
std::vector<WaterOutlet> outlets;
std::vector<WaterPort<WaterOutlet>> ports;

void wp_setup()
{
  for (auto &port : ports)
  {
    port.setup();
  }
}

void wp_report_config()
{
  StaticJsonDocument<1024> doc;
  auto array = doc.to<JsonArray>();

  for (auto &port : ports)
  {
    auto obj = array.createNestedObject();
    obj["outlet_id"] = port.outlet_id;
    port.print(obj);
  }


  serial_and_mqtt_event(doc);
}


void wp_report_measurements()
{

  StaticJsonDocument<2048> doc;
  auto obj = doc.to<JsonObject>();

  auto array = obj.createNestedArray("ports");
  for (auto &port : ports)
  {
    auto portObj = array.createNestedObject();
    portObj["v"] = port.avg.getAverage();
    portObj["s"] = port.needsWater()?1:0;
  }

  serial_and_mqtt_out(doc);
}

static waitTimer<waitNextMeasurement * 1000> last_measurement;
static waitTimer<waitAfterWatering * 1000> last_processing;

void wp_measure()
{
    for (auto &port : ports) { port.update(); delay(10); }
}
void wp_loop()
{
  // put your main code here, to run repeatedly:

  for (auto &outlet : outlets)  { outlet.process(); }
  for (auto &pump : pumps)  { pump.process(); }

  if (last_measurement.check_restart())
  {
    wp_measure();
    wp_report_measurements();

    if (last_processing.check_restart())
    {
      for (auto &port : ports) { port.process(); }
    }
  }
}

/*
How to detect pump issues:
- Blocked / No water: After a certain period, water flow must reach a certain level, stop pumping, notify, retry later
- Possible tube breakage: water flow exceed threshold -> stop, notify, do not retry later
*/
#include <Adafruit_INA219.h>
Adafruit_INA219 ina219;

void setup()
{

  led_warn(true); // turn on LED while running init code

  pinMode(BUTTON_RESET, INPUT_PULLUP);

  EEPROM.begin(512);

  Serial.begin(115200);
  Serial.println("Setup");

  mqtt_setup();
  mqtt_loop(); // we do a first run as it will connect us to mqtt server

  // dynamically create (most) relvant objects (Valves, Pumps, Outlets, Ports)
  // just flowmeter is a static object due to ISR
  // things are a bit hardwired at the moment, but this can be changed to create
  // all connections dynamically

  // we must fill a vector completely before passing around object references
  for (auto  pin : pumpPins)               { pump_switches.emplace_back(pin, true); }
  for (auto& pump_switch : pump_switches) { pumps.emplace_back(pump_switch, fm); }
  for (auto  pin : valvePins)              { valves.emplace_back(pin, true); }
  for (auto& valve : valves)              { outlets.emplace_back(pumps[0], valve); }

  {
    size_t outlet_idx = 0;
    for (auto pin : sensorPins)
    {
      outlet_idx++;
      ports.emplace_back(pin, outlet_idx);
    }
  }

  wp_setup();
  wp_report_config();

  attachInterrupt(fm.isr_pin(), fm_isr, fm.isr_mode());
  led_warn(false);

  Serial.println(ina219.begin()?"INA219: Found!":"INA219: Failed");
}

bool pumps_reset()
{
  bool did_reset = false;

  for (auto &outlet : outlets)
  {
    if (outlet.has_issue())
    {
      outlet.reset();
    }
  }
  for (auto &elem : pumps)
  {
    if (elem.has_issue())
    {
      elem.reset();
      did_reset = true;
    }
  }
  
  return did_reset;
}

void led_warn_loop()
{
  static waitTimer<500> when_blink_red;

  static bool led_warn_state = false;
  static bool has_issue = false;

  if (when_blink_red.check_restart())
  {
    has_issue = false;
    for (auto &pump : pumps)
    {
      has_issue |= pump.has_issue();
    }

    led_warn_state = has_issue ? (!led_warn_state) : LOW;

    led_warn(led_warn_state);
  }

  // button == LOW -> button has been pressed
  if (digitalRead(BUTTON_RESET) == LOW)
  {
    if (has_issue == true)
    {    
      if (pumps_reset())  { serial_and_mqtt_event("CMD_RESET_DONE"); }
    }
    else
    {
      wp_measure();
      wp_report_measurements();
      serial_and_mqtt_event("CMD_MEASURE_DONE");
    }
  }
}

void loop()
{
  led_warn_loop();
  mqtt_loop();
  wp_loop();

  static waitTimer<10> last_current;
  static AverageBuffer<float,8> avg_current;
  static bool pump_on = false;
  static std::array<char,16> pattern = { '?', '?','?','?','?','?','?','?','?', '?','?','?','?','?','?','?'};
  static bool was_ok = false;
  static bool was_empty = false;

  if (pumps[0].is_active() && last_current.check_restart())
  {
    float current_mA = ina219.getCurrent_mA();
    uint64_t now = millis();
    if (pump_on == false)
    {
        avg_current.reset();
        pump_on = true;
    }
    avg_current.append(current_mA);
    if (avg_current.hasAverage())
    {
      float avg = avg_current.getAverage();
      Serial.printf("Current: %f at %Ld\n",avg_current.getAverage(),now);
      

      // shift one entry to right;
      for (int idx = sizeof(pattern)/sizeof(pattern[0]) - 1; idx >0; idx--)
      {
        pattern[idx] = pattern[idx-1];
      }
      if (avg > 550)
      {
        pattern[0] = 'X';
      }
      else if (avg > 450)
      {
        pattern[0] = '+';
      }
      else if (avg > 305)
      {
        pattern[0] = 'o';
      }
      else if (avg > 280)
      {
        pattern[0] = '|';
      }
      else if (avg > 80)
      {
        pattern[0] = '-';
      }
      else if (avg > 80)
      {
        pattern[0] = 'x';
      }
      for (auto pv: pattern)
      {
        Serial.print(pv);
      }


      // have we passed the startup time 
      // indicated by having filled all values of pattern with
      // real current assessments?
      if (pattern.back() != '?')
      {
          bool is_ok = std::find_if(pattern.begin(), pattern.end()-1, [] (const char& o) { return o != 'o'; }) == pattern.end()-1;
          if (is_ok)
          { 
            Serial.print(" OK!");
            was_ok = true;
          }
          else
          {
            Serial.print(" PROBLEM!");
            if (was_ok == true)
            {
              pumps[0].set_issue();
              was_ok = false;
            }
          }
      }

      Serial.println();


      avg_current.reset();
      
    }
  }
  else 
  {
    
    bool new_pump_on = pumps[0].is_active();
    if (new_pump_on != pump_on)
    {
      // state change detected
      if (new_pump_on == true)
      {
        // was_ok = false;
      }
      else
      {
        // was_ok = false;
        // reset to initial pattern
        for (auto& c: pattern)
        {
          c = '?';
        }
        
      }
      pump_on = new_pump_on;
    }
  }
}


void mqtt_process_in(const char *jsonIO)
{
  StaticJsonDocument<1024> doc;

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, jsonIO);
  bool nocmd = true;
  // Test if parsing succeeds.
  if (error)
  {
    Serial.printf("JSON ERROR IN: `%s`", jsonIO);
  }
  else
  {
    uint16_t dose = doc["dose"];
    if (dose > 0)
    {
      nocmd = false;
      uint16_t outlet = doc["outlet"];
      
      if (outlet <= outlets.size() && outlet > 0)
      {
        outlets[outlet - 1].doseStart(dose);
        serial_and_mqtt_event("CMD_OK");
      }
      else
      {
        serial_and_mqtt_event("ERROR_WRONGOUTLETID");
      }
    }

    {
      JsonVariantConst trigger_measurement = doc["measure"];
      if (trigger_measurement.isNull() == false)
      {
        nocmd = false;
        if (trigger_measurement.is<bool>() && trigger_measurement.as<bool>() == true)
        {
            wp_measure();
            wp_report_measurements();
            serial_and_mqtt_event("CMD_MEASURE_DONE");
        }
        else
        {
          serial_and_mqtt_event("CMD_MEASURE_NEEDS_TRUE");
        }
      }
    }

    {
      JsonVariantConst reset_issue = doc["reset"];
      if (reset_issue.isNull() == false)
      {
        nocmd = false;
        if (reset_issue.is<bool>() && reset_issue.as<bool>() == true)
        {
          if (pumps_reset())  { serial_and_mqtt_event("CMD_RESET_DONE"); }
        }
        else
        {
          serial_and_mqtt_event("CMD_RESET_NEEDS_TRUE");
        }
      }
    }

    {
      JsonVariantConst setup_time = doc["time"];
      if (setup_time.isNull() == false)
      {
        nocmd = false;
        if (setup_time.is<bool>() && setup_time.as<bool>() == true)
        {
          if (setup_rtc())
          {
            print_time();
          }
          else
          {
            serial_and_mqtt_event("ERROR_NO_TIME");
          }
        }
        else
        {
          serial_and_mqtt_event("CMD_TIME_NEEDS_TRUE");
        }
      }
    }

    {
      JsonObject cmd = doc["set"];
      if (cmd.isNull() == false)
      {
        nocmd = false;
        uint16_t target = cmd["target"];
        const char *property = cmd["property"];
        uint32_t value = cmd["value"];
        bool cmdok = false;
        if (target > 0 && target <= ports.size() && property != NULL)
        {
          auto *port = &ports[target - 1];
          auto lambdaDose = [port](uint32_t value)
          {
            port->setDose(value);
            return true;
          };
          auto lambdaLimit = [port](uint32_t value)
          {
            port->setLimit(value);
            return true;
          };
          auto lambdaPump = [port](uint32_t value)
          {
            bool retval = false;
            if (value <= pumps.size())
            {
              port->setOutlet(value);
              retval = true;
            }
            return retval;
          };

          switch (property[0])
          {
          case 'd': // dose
            cmdok = lambdaDose(value);
            break;
          case 'l': // limit
            cmdok = lambdaLimit(value);
            break;
          case 'p': // pump
            cmdok = lambdaPump(value);
            break;
          default:
            Serial.println(F("JSON failed: unknown cmd property (use \"property\" : \"d[ose]\",\"l[imit]\", \"p[ump]\") "));
            break;
          }
          if (cmdok)
          {
            serial_and_mqtt_event("CMD_SET_OK");
            // HACK
            EEPROM.commit();
          }
          else
          {
            serial_and_mqtt_event("CMD_SET_ERROR_VAL_RANGE");
          }
        }
        else
        {
          serial_and_mqtt_event("CMD_SET_ERROR_TARGET_OR_ARGS_INVALID");
        }
      }
    }

    if (nocmd == true)
    {
      serial_and_mqtt_event("CMD_NOCMD_ERROR");
    }
  }
}
